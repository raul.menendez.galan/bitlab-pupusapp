package sv.edu.bitlab.pupusap

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "arroz"
//private const val ARG_PARAM2 = "maiz"
const val CONTADOR_ARROZ = "ARROZ"
const val CONTADOR_MAIZ = "MAIZ"
/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OrdenFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [OrdenFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OrdenFragment : Fragment() {
  // TODO: Rename and change types of parameters
  var arroz = arrayListOf<Int>()
  var maiz = arrayListOf<Int>()
  //private var param1: String? = null
  //private var param2: String? = null
  val lineItemsIDs = arrayOf(
    arrayOf(R.id.lineItemDetail1, R.id.lineItemPrice1),
    arrayOf(R.id.lineItemDetail2, R.id.lineItemPrice2),
    arrayOf(R.id.lineItemDetail3, R.id.lineItemPrice3),
    arrayOf(R.id.lineItemDetail4, R.id.lineItemPrice4),
    arrayOf(R.id.lineItemDetail5, R.id.lineItemPrice5),
    arrayOf(R.id.lineItemDetail6, R.id.lineItemPrice6)
  )

  var loadingContainer : View?= null
  var confirmaOrden : Button?=null
  var mHandler: Handler?=null
  var i:Int=1
  val mRunnable = object :Runnable{
    override fun run() {
      mHandler!!.postDelayed(this,5000)
      cambio()
    }
  }

  var lytOrden: LinearLayout?=null

  private var listener: OnFragmentInteractionListener? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
     // if (arguments != null){
        arroz = it.getIntegerArrayList(CONTADOR_ARROZ)!!
        maiz = it.getIntegerArrayList(CONTADOR_MAIZ)!!
      //}

    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {

    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_orden, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    //Toast.makeText(this, "onCreate()", Toast.LENGTH_LONG).show()
    /*val params= this.activity?.intent?.extras
    arroz = params!!.getIntegerArrayList(CONTADOR_ARROZ)!!
    maiz = params.getIntegerArrayList(CONTADOR_MAIZ)!!
     */

    displayDetalle()

    Log.d("ACTIVITY", "onCreate()")
    //addFragment()

    loadingContainer = view.findViewById(R.id.cargarContainer)
    confirmaOrden = view.findViewById(R.id.button_confirma)
    lytOrden = view.findViewById<LinearLayout>(R.id.orden_enviada)
    mHandler = Handler(Looper.getMainLooper())

    confirmaOrden!!.setOnClickListener{
      mHandler!!.post(mRunnable)

    }

    loadingContainer!!.setOnClickListener{ showLoading(false)
    }
  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   *
   *
   * See the Android Training lesson [Communicating with Other Fragments]
   * (http://developer.android.com/training/basics/fragments/communicating.html)
   * for more information.
   */
  interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

  companion object {
    const val QUESO = 0//3
    const val FRIJOLES = 1//4
    const val REVUELTAS = 2//5
    const val QUESO_MAIZ = 3//3
    const val FRIJOLES_MAIZ = 4//4
    const val REVUELTAS_MAIZ = 5//5
    const val CONTADOR_ARROZ = "ARROZ"
    const val CONTADOR_MAIZ = "MAIZ"
    const val VALOR_PUPUSA = 0.5F

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdenFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(arroz: ArrayList<Int>, maiz: ArrayList<Int>) =
      OrdenFragment().apply {
        arguments = Bundle().apply {
          putIntegerArrayList(CONTADOR_ARROZ, arroz)
          putIntegerArrayList(CONTADOR_MAIZ, maiz)
        }
      }
  }
  fun displayDetalle() {
    val arr = arroz + maiz
    var total = 0.0f
    for((index, contador) in arr.withIndex()){
      val ids = lineItemsIDs[index]
      val detailTexview = view!!.findViewById<TextView>(ids[0])
      val priceTextView= view!!.findViewById<TextView>(ids[1])
      if(contador > 0){
        val totalUnidad = contador * VALOR_PUPUSA
        val descripcion = getDescripcion(index)
        detailTexview.text = getString(R.string.pupusa_line_item_description,
          contador, descripcion)
        total += totalUnidad
        val precio = DecimalFormat("$#0.00").format(totalUnidad)
        priceTextView.text = precio
      } else{
        detailTexview.visibility = View.GONE
        priceTextView.visibility = View.GONE
      }
    }
    val totalPrecio = view!!.findViewById<TextView>(R.id.lineItemPriceTotal)
    val precio = DecimalFormat("$#0.00").format(total)
    totalPrecio.text = precio

  }
  fun getDescripcion(index: Int): String {
    return when(index){
      QUESO -> "Queso de arroz"
      FRIJOLES -> "Frijol con queso de arroz"
      REVUELTAS -> "Revueltas de arroz"
      QUESO_MAIZ-> "Queso de maiz"
      FRIJOLES_MAIZ -> "Frijol con queso de maiz"
      REVUELTAS_MAIZ -> "Revueltas de maiz"
      else -> throw RuntimeException("Pupusa no soportada")
    }
  }

  //Funcion del cambio de pantalla de detalle orden a cargando y pantalla verde
  fun cambio(){
    when(i){
      1->{
        showLoading(true)
        i++
      }
      2->{
        val intent = Intent(activity, Orden_enviadaActivity::class.java)
        this.startActivity(intent)
        mHandler!!.removeCallbacks(mRunnable)
        i=1
      }
    }
  }

  /*private fun Intent(ordenFragment: OrdenFragment, java: Class<Orden_enviadaActivity>): Intent? {
    intent=Orden_enviadaActivity()
    return

  }*/

  fun showLoading(show: Boolean){
    var visibility= if (show) {
      View.VISIBLE
    }
    else View.GONE
    loadingContainer!!.visibility=visibility
  }
}
